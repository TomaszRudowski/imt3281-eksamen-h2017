package no.ntnu.imt3281.demo;

import no.ntnu.imt3281.movieExplorer.FilmDatabase;

/**
 * Class to create local derby database, if db corupted remove FilmDB folder and run
 * this to start clean database with empty tables.
 * @author Tomasz Rudowski
 *
 */
public class CreateLocalDB {
	public static void main(String[] args) {
		FilmDatabase filmDB = new FilmDatabase();
		filmDB.openConnectionAndCreateDB();
		filmDB.closeConnection();
	}
}
