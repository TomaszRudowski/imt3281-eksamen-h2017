package no.ntnu.imt3281.movieExplorer;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * There will be more classes that uses API requests, I decided to create an interface for common methods
 * @author Tomasz Rudowski
 *
 */
public interface RequestAPI {
	
	/**
	 * sends GET request and expects json response to create JSON object
	 * @param reqURL
	 * @return JSON created from response
	 */
	static JSON sendRequest(String reqURL) {
		JSON toReturn = null;
		// from CallTheMovieDB.java example
		try {
			String response = Unirest.get(reqURL).asString().getBody();
			//System.out.println(response);		// Outputs JSON response from server.
			//Unirest.shutdown();			// Must be called at the end of the application
			
			JSONParser parser = new JSONParser();
			JSONObject jsonResp = (JSONObject) parser.parse(response);
			toReturn = new JSON(jsonResp);
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return toReturn;
	}
}
