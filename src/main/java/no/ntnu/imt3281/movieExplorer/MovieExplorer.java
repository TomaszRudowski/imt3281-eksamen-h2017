package no.ntnu.imt3281.movieExplorer;

import java.io.File;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class MovieExplorer extends Application {

	private static Stage primaryStage;
	
	private static String selectedDirectoryPath;
	
	@Override
	public void start(Stage prima) throws Exception {
		primaryStage = prima;
		primaryStage.setTitle("Movie explorer");
		BorderPane mainFrame = FXMLLoader.load(getClass().getResource("GUI.fxml"));
	    
	    Scene myScene = new Scene(mainFrame);
	    primaryStage.setScene(myScene);
	    primaryStage.show();
	}
	public static void main(String[] args) {
		launch(args);
	}
	/**
	 * help function to get access to primary stage form GUI.java
	 * idea inspired from: https://stackoverflow.com/questions/33932309/how-to-access-a-javafx-stage-from-a-controller
	 * @return
	 */
	static public Stage getPrimaryStage() {
        return MovieExplorer.primaryStage;
    }
	
	
	public static String getSelectedDirectory() {
		return selectedDirectoryPath;
	}
	
	public static void setSelectedDirectory(String selectedDirectory) {
		MovieExplorer.selectedDirectoryPath = selectedDirectory;
	}
}
