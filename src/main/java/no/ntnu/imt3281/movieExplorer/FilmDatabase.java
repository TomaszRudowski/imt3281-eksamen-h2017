package no.ntnu.imt3281.movieExplorer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

/**
 * Class handling Derby DB instance FilmDB, based on code worked in project 2
 * @author Tomasz Rudowski
 *
 */
public class FilmDatabase {
	private static final Logger LOGGER = Logger.getLogger( FilmDatabase.class.getName() );
	private final String url = "jdbc:derby:FilmDB";
	private static Connection dbConnection;
	
	/**
	 * Opens a connection with create=true parameter, if DB exists fails to create tables
	 * but connect to existing DB (with a warning), used only once at program start
	 * later use openConnection()
	 * @return false if not connected
	 */
	public boolean openConnectionAndCreateDB() {
		try {
			dbConnection = DriverManager.getConnection(url + ";create=true");
			// here if newly created db
			addGenreTable();
			addActorsTable();	// #8
			addPartTable();		// #8
			addMoviesTable();	// #8
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
			return false;
		}		
		return true;
	}
	
	/** 
	 * Opens connection to database
	 */
	public void openConnecion() {		
		try {
			dbConnection = DriverManager.getConnection(url);
		} catch (SQLException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		
	}
	
	/**
	 * Closes connection to database
	 */
	public void closeConnection () {
		try {
			dbConnection.close();
		} catch (SQLException e) {			
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		
	}

	/**
	 * Create genres table in db
	 */
	private void addGenreTable() {
		try (Statement stmt = dbConnection.createStatement()) {			
			stmt.execute("CREATE TABLE genres (id int NOT NULL, name varchar(64) NOT NULL, PRIMARY KEY  (id))");
			LOGGER.log(Level.INFO, "Table GENRES created");
		} catch (SQLException e) {			
			LOGGER.log(Level.FINE, e.toString(), e); // Fine because DB exists.
		}
	}
	
	/**
	 * Inserts genre data to genres table
	 * @param id
	 * @param name
	 * @return number of added rows, -1 if SQLException
	 */
	public int addToGenresTable (int id, String name) {
		int addedRows = 0;
		try (PreparedStatement stmt = dbConnection.prepareStatement("INSERT INTO genres (id,name) VALUES (?, ?)")) {
			stmt.setInt(1, id);
			stmt.setString(2, name);
			addedRows = stmt.executeUpdate();
		} catch (SQLException e) {
			// here if duplicate id
			//LOGGER.log(Level.WARNING, e.toString(), e);
			return -1;
		}
		return addedRows;		
	}
	
	/**
	 * Finds genre name in db based on given id
	 * @param id
	 * @return genre name as string, null if not found or SQLException
	 */
	public String getGenreName (int id) {
		String returnString = null;
		try  {
			PreparedStatement stmt = dbConnection.prepareStatement("SELECT * FROM genres WHERE id=?");
			stmt.setInt(1, id);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				returnString = result.getString("name");
			} else {
				LOGGER.log(Level.INFO, "genre not found");
			}
		} catch (SQLException e) {
			returnString = null;
			//LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		return returnString;
	}
	
	/**
	 * #8 creates a table for storing data from API
	 * for Search.multiSearch(name) data
	 */
	private void addMoviesTable() {
		try (Statement stmt = dbConnection.createStatement()) {			
			stmt.execute("CREATE TABLE movies (name varchar(256) NOT NULL, json LONG VARCHAR NOT NULL, PRIMARY KEY (name))");
			LOGGER.log(Level.INFO, "Table MOVIES created");
		} catch (SQLException e) {			
			LOGGER.log(Level.FINE, e.toString(), e); // Fine because DB exists.
		}		
	}

	/**
	 * #8 creates a table for storing data from API
	 * for Search.takesPartIn(id) data
	 */
	private void addPartTable() {
		try (Statement stmt = dbConnection.createStatement()) {			
			stmt.execute("CREATE TABLE parts (id int NOT NULL, json LONG VARCHAR NOT NULL, PRIMARY KEY (id))");
			LOGGER.log(Level.INFO, "Table PARTS created");
		} catch (SQLException e) {			
			LOGGER.log(Level.FINE, e.toString(), e); // Fine because DB exists.
		}
	}

	/**
	 * #8 creates a table for storing data from API
	 * for Search.actors(id) data
	 */
	private void addActorsTable() {
		try (Statement stmt = dbConnection.createStatement()) {			
			stmt.execute("CREATE TABLE actors (id int NOT NULL, json LONG VARCHAR NOT NULL, PRIMARY KEY (id))");
			LOGGER.log(Level.INFO, "Table ACTORS created");
		} catch (SQLException e) {			
			LOGGER.log(Level.FINE, e.toString(), e); // Fine because DB exists.
		}
	}
	
	/**
	 * #8 adding response json as a string, it will be easy to recreate JSON from it when SELECT be called
	 * @param name
	 * @param data
	 * @return
	 */
	public int addToMoviesTable (String name, JSON data) {
		int addedRows = 0;
		try (PreparedStatement stmt = dbConnection.prepareStatement("INSERT INTO movies (name,json) VALUES (?, ?)")) {
			stmt.setString(1, name);
			JSONObject json = (JSONObject) data.getDataObject();			
			stmt.setString(2, json.toString());
			addedRows = stmt.executeUpdate();
		} catch (SQLException e) {
			// here if duplicate name
			LOGGER.log(Level.WARNING, e.toString(), e);
			return -1;
		}
		return addedRows;		
	}
	/**
	 * #8 gets data from db and transforms it into JSON object
	 * @param name
	 * @return
	 */
	public JSON getFromMovies (String name) {
		JSON returnJSON = null;
		try  {
			PreparedStatement stmt = dbConnection.prepareStatement("SELECT * FROM movies WHERE name LIKE ?");
			stmt.setString(1, name);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				returnJSON = new JSON(result.getString("json"));
			} else {
				LOGGER.log(Level.INFO, "Search.multiSearch object not found in db");
			}
		} catch (SQLException e) {			
			//LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		return returnJSON;
	}
	
	/**
	 * #8 adding response json as a string, it will be easy to recreate JSON from it when SELECT be called
	 * @param name
	 * @param data
	 * @return
	 */
	public int addToPartsTable (int id, JSON data) {
		int addedRows = 0;
		try (PreparedStatement stmt = dbConnection.prepareStatement("INSERT INTO parts (id,json) VALUES (?, ?)")) {
			stmt.setInt(1, id);
			JSONObject json = (JSONObject) data.getDataObject();			
			stmt.setString(2, json.toString());
			addedRows = stmt.executeUpdate();
		} catch (SQLException e) {
			// here if duplicate name
			LOGGER.log(Level.WARNING, e.toString(), e);
			return -1;
		}
		return addedRows;		
	}
	/**
	 * #8 gets data from db and transforms it into JSON object
	 * @param name
	 * @return
	 */
	public JSON getFromParts (int id) {
		JSON returnJSON = null;
		try  {
			PreparedStatement stmt = dbConnection.prepareStatement("SELECT * FROM parts WHERE id=?");
			stmt.setInt(1, id);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				returnJSON = new JSON(result.getString("json"));
			} else {
				LOGGER.log(Level.INFO, "Search.takesPartIn object not found in db");
			}
		} catch (SQLException e) {			
			//LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		return returnJSON;
	}
	
	/**
	 * #8 adding response json as a string, it will be easy to recreate JSON from it when SELECT be called
	 * to consider: refactor PARTS and ACTORS functions are very similar
	 * @param name
	 * @param data
	 * @return
	 */
	public int addToActorsTable (long id, JSON data) {
		int addedRows = 0;
		try (PreparedStatement stmt = dbConnection.prepareStatement("INSERT INTO actors (id,json) VALUES (?, ?)")) {
			stmt.setLong(1, id);
			JSONObject json = (JSONObject) data.getDataObject();			
			stmt.setString(2, json.toString());
			addedRows = stmt.executeUpdate();
		} catch (SQLException e) {
			// here if duplicate name
			LOGGER.log(Level.WARNING, e.toString(), e);
			return -1;
		}
		return addedRows;		
	}
	/**
	 * #8 gets data from db and transforms it into JSON object
	 * to consider: refactor PARTS and ACTORS functions are very similar
	 * @param name
	 * @return
	 */
	public JSON getFromActors (long id) {
		JSON returnJSON = null;
		try  {
			PreparedStatement stmt = dbConnection.prepareStatement("SELECT * FROM actors WHERE id=?");
			stmt.setLong(1, id);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				returnJSON = new JSON(result.getString("json"));
			} else {
				LOGGER.log(Level.INFO, "Search.takesPartIn object not found in db");
			}
		} catch (SQLException e) {			
			//LOGGER.log(Level.SEVERE, e.toString(), e);
		}
		return returnJSON;
	}
	/**
	 * #10 used to flush all data from database, used more lines, problem with multiply statement
	 * inspired by https://db.apache.org/derby/docs/10.8/ref/rrefsqljtruncatetable.html 
	 */
	public void emptyDatabase() {	
		LOGGER.log(Level.INFO, "emptyDB called");
		try {
			PreparedStatement stmt = dbConnection.prepareStatement("TRUNCATE TABLE movies");
			stmt.executeUpdate();
			stmt = dbConnection.prepareStatement("TRUNCATE TABLE parts");
			stmt.executeUpdate();
			stmt = dbConnection.prepareStatement("TRUNCATE TABLE actors");
			stmt.executeUpdate();
			stmt = dbConnection.prepareStatement("TRUNCATE TABLE genres");
			stmt.executeUpdate();
			LOGGER.log(Level.INFO, "All tables cleared");
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, e.toString(), e);
		}
				
	}
}
