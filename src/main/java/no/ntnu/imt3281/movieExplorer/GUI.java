package no.ntnu.imt3281.movieExplorer;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Pair;

public class GUI {
    @FXML private TextField searchField;
    @FXML private TreeView<SearchResultItem> searchResult;
    
    @FXML private Pane detailPane;
    private FXMLLoader detailPaneLoader;
    
    @FXML private MenuItem preferences;
    

    @FXML private MenuItem about;
    
    // Root node for search result tree view
    private TreeItem<SearchResultItem> searchResultRootNode = new TreeItem<SearchResultItem> (new SearchResultItem(""));
    
    @FXML
    /**
     * Called when the object has been created and connected to the fxml file. All components defined in the fxml file is 
     * ready and available.
     */
    public void initialize() {
    	searchResult.setRoot(searchResultRootNode);
    		
    	// Task #5 COMMENTED TO FOLLOW #6 INSTRUCTIONS
  		// from https://stackoverflow.com/questions/13857041/tree-item-select-event-in-javafx2 and JavaFX docs
    	//	searchResult.getSelectionModel()
        //    .selectedItemProperty()
        //    .addListener((observable, oldValue, newValue) -> System.out.println("Selected Text : " + newValue.getValue().media_type));
  		// end of copy
    		
    	// Task #6
    	searchResult.getSelectionModel()
            .selectedItemProperty()
            .addListener((observable, oldValue, selectedElem) -> {
            	if (selectedElem.getValue().media_type.equals("person")) {
            		int personId = (int) selectedElem.getValue().id;
            		JSON results = Search.takesPartIn(personId).get("results");
            		// selectedElem is a TreeItem<SearchResultItem> that will be a root for a new TreeView
            		for (int i=0; i<results.size(); i++) {            			
            			// to do: one more Search method to use discover tv API
            			SearchResultItem item = new SearchResultItem(results.get(i), "movie");
            			//System.out.println("type: " + item.title);
                		selectedElem.getChildren().add(new TreeItem<SearchResultItem>(item));                	           			
            		}            		
            	} else { // #6 'movie' and 'tv' items
            		long filmID = (long) selectedElem.getValue().id;            		
            		JSON results = Search.actors(filmID).get("cast");            		
            		for (int i=0; i<results.size(); i++) {
            			SearchResultItem item = new SearchResultItem(results.get(i), "person");            			
                		selectedElem.getChildren().add(new TreeItem<SearchResultItem>(item));                	          			
            		}
            		// TASK #7
            		showFilmDetails(filmID);
            	}
            	selectedElem.setExpanded(true);
            	
            	});
    		
    }
	
    /** #7
     * 
     * @param filmID
     */
    private void showFilmDetails(long filmID) {
    	JSON detailJSON = Search.movie(filmID);
    	//System.out.println(detailJSON.getDataObject()); 
    	
    	detailPaneLoader = new FXMLLoader(getClass().getResource("Details.fxml"));
    	
    	try {
			AnchorPane filmPane = detailPaneLoader.load();	
			DetailsController controller = detailPaneLoader.getController();
			controller.displayDetails(detailJSON);
			detailPane.getChildren().clear();
			detailPane.getChildren().add(filmPane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    /**
     * #9 to run when menuItem 'preferanser' chosen
     * using MovieExplorer.getPrimaryStage() to get access to main frame window
     * needed as parameter to DirectoryChooser dialog
     * @param event
     */
    @FXML
    void chooseFolder(ActionEvent event) {
    	// from: https://stackoverflow.com/questions/33932309/how-to-access-a-javafx-stage-from-a-controller
    	Stage main = MovieExplorer.getPrimaryStage(); 
    	// end of copy
    	
    	Window mainWindow = main.getOwner();
    	
    	// from http://java-buddy.blogspot.no/2013/03/javafx-simple-example-of.html
    	 DirectoryChooser directoryChooser = new DirectoryChooser();
         File selectedDirectory = directoryChooser.showDialog(mainWindow);         
         // end of copy
         
         // temp, for testing
         if(selectedDirectory == null){
             System.out.println("No Directory selected");
         }else{    
        	 String rootPath = selectedDirectory.getAbsolutePath().replace('\\', '/');		
        	 // replace \ with / to avoid evt platform issues
        	 createSubfolders(rootPath);
        	 MovieExplorer.setSelectedDirectory(rootPath);
         }    	
    }
    
    /**
     * #9 create subfolders based on root folder absolute path
     * @param rootPath 
     */
	private void createSubfolders(String rootPath) {
		
		//  w1280, w500, w780, h623, w300
		String [] subfolders = {"/w300", "/h632", "/w780", "/w500", "/w1280"};
		for (int i = 0 ; i < 5 ; i++) {
			String subfolderPath = rootPath + subfolders[i];
			// from https://stackoverflow.com/questions/25174784/how-to-create-a-directory-and-sub-directory-structure-with-java
			File newDir = new File(subfolderPath);	
			if (!newDir.exists()) {
				newDir.mkdir();		// not neccessary with mkdirs(), root folder must exist!!
				//System.out.println("created " + subfolderPath);
			} else {
				//System.out.println("exist " + subfolderPath);
			}
		}
	}
	
	/**
	 * #10 action handler after choosing Hjelp->Om ME menu
	 * @param event
	 */
	@FXML
    void showAboutDialog(ActionEvent event) {
		// http://code.makery.ch/blog/javafx-dialogs-official/
		// Create the custom dialog.
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle("Om MovieExplorer");
		dialog.setHeaderText("Om MovieExplorer");
		
		// Set the button types.
		ButtonType okButtonType = new ButtonType("OK", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(okButtonType);
								
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));
		// end of copied code changed text input fields from original source to fit new layout
		
		// Create the labels
		Label dbSize = new Label(getSizeOfDB());
		Label posterSize = new Label(getSizeOfSelectedFolder("/w780"));
		/*
		Label profSize = new Label("");
		Label backSize = new Label("");
		Label logoSize = new Label("");
		Label stillSize = new Label("");
		*/
		Label profSize = new Label(getSizeOfSelectedFolder("/h632"));
		Label backSize = new Label(getSizeOfSelectedFolder("/w1280"));
		Label logoSize = new Label(getSizeOfSelectedFolder("/w500"));
		Label stillSize = new Label(getSizeOfSelectedFolder("/w300"));
		
		Button emptyDB = new Button("Tom database");		
		emptyDB.setDisable(false);
		// a line from: https://stackoverflow.com/questions/22744334/button-onactionproperty-addlistener-is-not-working-in-javafx
		emptyDB.addEventHandler(ActionEvent.ACTION, e -> {
			FilmDatabase fdb = new FilmDatabase();
			fdb.openConnecion();
			fdb.emptyDatabase();
			fdb.closeConnection();
			});
		
		Button emptyCache = new Button("Tom cache");		
		emptyCache.setDisable(false);	
		emptyCache.addEventHandler(ActionEvent.ACTION, e2 -> {	
			// to do: empty cache	
			cleanCurrentDirectory();
		});
		grid.add(new Label("Databasen:"), 0, 0);
		grid.add(dbSize, 1, 0);
		grid.add(new Label("Posterbilder:"), 0, 1);
		grid.add(posterSize, 1, 1);
		grid.add(new Label("Profilbilder:"), 0, 2);
		grid.add(profSize, 1, 2);
		grid.add(new Label("Backdropbilder:"), 0, 3);
		grid.add(backSize, 1, 3);
		grid.add(new Label("Logobilder:"), 0, 4);
		grid.add(logoSize, 1, 4);
		grid.add(new Label("Stillbilder:"), 0, 5);
		grid.add(stillSize, 1, 5);
		grid.add(emptyDB, 0, 6);
		grid.add(emptyCache, 1, 6);
				
		dialog.getDialogPane().setContent(grid);
		dialog.showAndWait();
	
    }





	@FXML
    /**
     * Called when the seqrch button is pressed or enter is pressed in the searchField.
     * Perform a multiSearch using theMovieDB and add the results to the searchResult tree view.
     * 
     * @param event ignored
     */
    void search(ActionEvent event) {
    		JSON result = Search.multiSearch(searchField.getText()).get("results");
    		TreeItem<SearchResultItem> searchResults = new TreeItem<> (new SearchResultItem("Searching for : "+searchField.getText()));
    		searchResultRootNode.getChildren().add(searchResults);
    		for (int i=0; i<result.size(); i++) {
    			SearchResultItem item = new SearchResultItem(result.get(i));
    			searchResults.getChildren().add(new TreeItem<SearchResultItem>(item));
    		}
    		searchResultRootNode.setExpanded(true);
    		searchResults.setExpanded(true);
    }
    
    class SearchResultItem {
    		private String media_type = "";
    		private String name = "";
    		private long id;
    		private String profile_path = "";
    		private String title = ""; 
    		
    		/**
    		 * Create new SearchResultItem with the given name as what will be displayed in the tree view.
    		 * 
    		 * @param name the value that will be displayed in the tree view
    		 */
    		public SearchResultItem(String name) {
    			this.name = name;
    		}
    		
    		/**
    		 * Create a new SearchResultItem with data form this JSON object.
    		 * 
    		 * @param json contains the data that will be used to initialize this object.
    		 */
		public SearchResultItem(JSON json) {
			
				media_type = (String) json.getValue("media_type");
				if (media_type.equals("person")) {
					name = (String)json.getValue("name");	
					profile_path = (String)json.getValue("profile_path");
				} else if (media_type.equals("movie")) {
					title = (String)json.getValue("title");
				} else if (media_type.equals("tv")) {
					// task #5 (tv title is stored in 'name' field -> found in original json from API)
					name = (String)json.getValue("name");		
				}
			
			id = (Long)json.getValue("id");
		}
		
		/**
		 * added constructor to realize #6 takesPartsIn.results have no media_type field
		 * temporary takesPartsIn call API to get 'movie' only, should have to functions 
		 * to fetch data from two APIs, one for 'movie' and one for 'tv'
		 * @param json
		 * @param type
		 */
		public SearchResultItem(JSON json, String type) {			
			if (type.equals("movie")) {
				media_type = "movie";
				title = (String)json.getValue("title");
			} else if (type.equals("tv")) {
				media_type = "tv";
				name = type;		
			} else if (type.equals("person")) {
				media_type = "person";
				name = (String)json.getValue("name");	
				profile_path = (String)json.getValue("profile_path");
			}
			
			id = (Long)json.getValue("id");
		}
    		
		/**
		 * Used by the tree view to get the value to display to the user. 
		 */
		@Override
		public String toString() {
			if (media_type.equals("person")) {
				return name;
			} else if (media_type.equals("movie")) {
				return title;
			} else {
				return name;
			}
		}	
    }
    
    /**
     * #10 calls help function to calculate a total of disk used.
     * subdir hardcoded values in dialog handler
     * @return
     */
    public String getSizeOfSelectedFolder(String subdir) {    	
    	long size = 0;
    	if (MovieExplorer.getSelectedDirectory() != null) {
    		size = getFolderSize(new File(MovieExplorer.getSelectedDirectory()+subdir));
    	}    	
    	return size + " B";
    }
    
    /**
     * #10 help funcion entirely from http://www.baeldung.com/java-folder-size
     * 
     * @param folder
     * @return
     */
    private long getFolderSize(File folder) {
        long length = 0;
        File[] files = folder.listFiles();
     
        int count = files.length;
     
        for (int i = 0; i < count; i++) {
            if (files[i].isFile()) {
                length += files[i].length();
            }
            else {
                length += getFolderSize(files[i]);
            }
        }
        return length;
    }
    
    /**
     * #10 path to db should be relative to current application folder
     * then can call getFolderSize(DBfolder)
     * to recieve size
     * @return
     */
	private String getSizeOfDB() {
		// TODO Auto-generated method stub
		/*String current;
		try {
			current = new java.io.File( "." ).getCanonicalPath();
			System.out.println("Current dir:"+current);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		File db = new File("FilmDB");		
		long size = getFolderSize(db);
		System.out.println(size);
		return size + " B";
	}
	
	/**
	 * #10 removes selected folder if not null
	 */
	private void cleanCurrentDirectory() {
		String root = MovieExplorer.getSelectedDirectory();
		if (root != null) {
			purgeDirectoryButKeepSubDirectories(new File(root+"/w780"));
			purgeDirectoryButKeepSubDirectories(new File(root+"/h632"));
			purgeDirectoryButKeepSubDirectories(new File(root+"/w1280"));
			purgeDirectoryButKeepSubDirectories(new File(root+"/w500"));
			purgeDirectoryButKeepSubDirectories(new File(root+"/w300"));
		}
	}
	
	/**
	 * * https://stackoverflow.com/questions/13195797/delete-all-files-in-directory-but-not-directory-one-liner-solution
	 * @param dir
	 */
	void purgeDirectoryButKeepSubDirectories(File dir) {
	    for (File file: dir.listFiles()) {
	        if (!file.isDirectory()) file.delete();
	    }		
	}
}
