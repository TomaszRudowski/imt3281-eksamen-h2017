package no.ntnu.imt3281.movieExplorer;

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Class for task 3, 
 * @author Tomasz Rudowski
 *
 */
public class Genres {
	private static FilmDatabase filmDB;
		
	/**
	 * try first to find data in database FilmDB, if not there runs update from API
	 * @param id
	 * @return
	 */
	public static String resolve(int id) {	
		filmDB = new FilmDatabase();
		filmDB.openConnecion();		
		// 1st data in db		
		String toReturn = filmDB.getGenreName(id); // null if not found
		
		if (toReturn == null) {
			// 2nd not in db, this may be first time run resolve..., fetch from API and add to db
			fetchFromAPI();
			toReturn = filmDB.getGenreName(id);
		}
			
		filmDB.closeConnection();
		
		return toReturn;
	}
	
	/**
	 * getting genre data from API
	 * to consider: !! @param id Add this param in case of testing for legal id at API
	 * todo: consider testing if it is legal id, it will never be added to db
	 * if not on API, ie. multiply call when tried again with the same id...
	 */
	private static void fetchFromAPI() {		
		// https://api.themoviedb.org/3/genre/movie/list?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US
		// https://api.themoviedb.org/3/genre/tv/list?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US
		 String reqMoviesGenre = "https://api.themoviedb.org/3/genre/movie/list?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US";
		 String reqTvGenre = "https://api.themoviedb.org/3/genre/tv/list?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US";
		 
		 fetchData(reqMoviesGenre);
		 fetchData(reqTvGenre);
		
	}

	/**
	 * Refactored to avoid duplication, runs GET request and inserts into database
	 * @param request
	 */
	private static void fetchData(String request) {
		JSONArray genreArray;
		JSON response = RequestAPI.sendRequest(request);
		if (response != null) {
			JSONObject mainJsonObj = (JSONObject) response.getDataObject();
			genreArray = (JSONArray) mainJsonObj.get("genres");		// if not null than data is array
			
			for (int i = 0 ; i < genreArray.size() ; i++) {
				JSONObject genreObj = (JSONObject) genreArray.get(i);
				filmDB.addToGenresTable((int)(long)genreObj.get("id"), (String)genreObj.get("name"));
			}			
		}
	}
	


}
