package no.ntnu.imt3281.movieExplorer;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.hamcrest.core.IsInstanceOf;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Class JSON for task 1, decided to go for Object data attribute after JSONArray/JSONObject issues
 * @author Tomasz Rudowski
 *
 */

public class JSON {
	
	private Object data;

	/**
	 * Constructor using string formatted in JSON friendly way, decided to go over to JSONArray
	 * because of JSONObject - JSONArray issues
	 * @param jsoninput
	 */
	public JSON(String jsoninput) {
		try {
			JSONParser parser = new JSONParser();		
			//JSONObject jsonObj = (JSONObject) parser.parse(jsoninput);
			data = (JSONObject) parser.parse(jsoninput);
			// from: https://stackoverflow.com/questions/18440098/org-json-simple-jsonarray-cannot-be-cast-to-org-json-simple-jsonobject
			//data = new JSONArray();
			//data.add(jsonObj);
			// end of copy, trying to go over to JSONArray	
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * used to go through the first get(stringKey), ie. JSON.dat should be JSONArray related with stringKey
	 * @param json
	 */
	public JSON(JSONArray jsonArray) {
		data = jsonArray;
	}
	
	/**
	 * Constructor used in the second step of get(key).get(key)
	 * @param jsonObj
	 */
	public JSON(JSONObject jsonObj) {
		//data = new JSONArray();
		//data.add(jsonObj);
		data = jsonObj;
	}

	/**
	 * used to receive a simple field value from a JSON object
	 * @param key
	 * @return
	 */
	public Object getValue(String key) {
		// TODO Auto-generated method stub			
		return ((JSONObject)data).get(key);
	}

	/**
	 * return JSON with data contains proper JSONArray related with stringKey, 
	 * this is first step to go through get(key).get(key)
	 * @param key
	 * @return
	 */
	public JSON get(String key) {				
		//return new JSON((JSONArray)data.get(key));	
		return new JSON((JSONArray)((JSONObject)data).get(key));
	}
	
	/**
	 * return JSONObject of given index, the second step to go through get(key).get(key)
	 * Expected to be called after the first get(stringKey), todo: security, limited access
	 * @param key
	 * @return
	 */
	public JSON get(int key) {				
		return new JSON( (JSONObject) ((JSONArray)data).get(key) );
	}
	
	
	/**
	 * print JSON object for testing
	 */
	public void printJSON () {
		if (data instanceof JSONArray) {
			printJSONArray((JSONArray)data, 0);
		} else {
			printJSONObject((JSONObject)data, 0);
		}
		
	}
	
	/**
	 * from example ParseJSON.java
	 * @param obj
	 * @param level
	 */
	public static void printJSONObject (JSONObject obj, int level) {
		String tabs = "";
		for (int i=0; i<level; i++) {
			tabs += "\t";
		}
		Set<String> keys = obj.keySet();
		for (String key : keys) {
	    		Object o = obj.get(key);
	    		if (o instanceof JSONArray) {	// Key points to a JSON array
	    			System.out.println (tabs+key+" [");
	    			printJSONArray((JSONArray)o, level+1);
	    			System.out.println (tabs+"]");
	    		} else if (o instanceof JSONObject) {	// Key points to a JSON object
	    			System.out.println (tabs+key+" -> ");
	    			printJSONObject((JSONObject)o, level+1);
	    		} else {	// Key points to a simple value
	    			System.out.println (tabs+key+": "+o);
	    		}
	    	}
	}
	
	/**
	 * from example ParseJSON.java
	 * @param arr
	 * @param level
	 */
	private static void printJSONArray(JSONArray arr, int level) {
		String tabs = "";
		for (int i=0; i<level; i++) {
			tabs += "\t";
		}
		Iterator<String> iterator = arr.iterator();
	    	while (iterator.hasNext()) {
	    		System.out.println(tabs+"[");
	    		Object o = iterator.next();
	    		if (o instanceof JSONArray) {	// Key points to a JSON array
	    			System.out.println (tabs+" [");
	    			printJSONArray((JSONArray)o, level+1);
	    			System.out.println (tabs+"]");
	    		} else if (o instanceof JSONObject) {	// Key points to a JSON object
	    			printJSONObject((JSONObject)o, level);
	    		} else {	// Key points to a simple value
	    			System.out.println (tabs+": "+o);
	    		}
	    		System.out.println(tabs+"],");
	    	}
	}

	/**
	 * @return size of data JSONArray, expected to be called after get(stringKey)
	 * first possibility: eg. parsedJSON.get("spoken_languages").size() just data.size()
	 * second: eg. parsedJSON.get("spoken_languages").get(0).size()
	 */
	public int size() {	
		//this.printJSON();
		if (data instanceof JSONArray) {
			return ((JSONArray)data).size();
		} else {
			return ((JSONObject)data).size();
		}			
	}
	
	/**
	 * used by Genre to get information stored in this.data during fetching json from API
	 * @return
	 */
	public Object getDataObject () {
		return data;
	}

}
