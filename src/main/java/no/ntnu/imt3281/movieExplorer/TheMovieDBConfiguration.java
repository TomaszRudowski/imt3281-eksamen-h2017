package no.ntnu.imt3281.movieExplorer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class TheMovieDBConfiguration {
	private JSONObject config;
	
	/**
	 * Create JSONObject config from string
	 * @param json 
	 */
	public TheMovieDBConfiguration(String json) {
		
		JSONParser parser = new JSONParser();
		try {
			config = (JSONObject) parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			config = null;
			e.printStackTrace();
		}	
	}
	

	/**
	 * not used, decided to skip it in task #7, possible in use later
	 * constructor that take no parameters gets config from API if not in db
	 * to do: check only once and put in database
	 * @param filmID
	 */
	@Deprecated
	public TheMovieDBConfiguration() {
		// https://api.themoviedb.org/3/configuration?api_key=a47f70eb03a70790f5dd711f4caea42d
		String req = "https://api.themoviedb.org/3/configuration?api_key=a47f70eb03a70790f5dd711f4caea42d";
		config = (JSONObject) RequestAPI.sendRequest(req).getDataObject();
	}



	/**
	 * Call a function to create URL based on file name and "backdrop_sizes" picture type
	 * @param fileName
	 * @return complete URL where picture is to be found
	 */
	public String getBackdropURL(String fileName) {
		return createURL("backdrop_sizes", fileName);
	}

	/**
	 * Call a function to create URL based on file name and "logo_sizes picture type
	 * @param fileName
	 * @return complete URL where picture is to be found
	 */
	public String getLogoURL(String fileName) {
		return createURL("logo_sizes", fileName);
	}

	/**
	 * Call a function to create URL based on file name and "poster_sizes" picture type
	 * @param fileName
	 * @return complete URL where picture is to be found
	 */
	public String getPosterURL(String fileName) {
		String rootPath = MovieExplorer.getSelectedDirectory();
		if (rootPath != null) {		// folder is selected, to do store in preferances
			String localPath = downloadFile("poster_sizes", fileName);
		}
		//System.out.println(MovieExplorer.getSelectedDirectory()+fileName);
		return createURL("poster_sizes", fileName);
	}

	/**
	 * Call a function to create URL based on file name and "profile_sizes" picture type
	 * @param fileName
	 * @return complete URL where picture is to be found
	 */
	public String getProfileURL(String fileName) {
		return createURL("profile_sizes", fileName);
	}

	/**
	 * Call a function to create URL based on file name and "still_sizes" picture type
	 * @param fileName
	 * @return complete URL where picture is to be found
	 */
	public String getStillURL(String fileName) {
		return createURL("still_sizes", fileName);
	}
	
	/**
	 * Common method for for all getXxxURL methods
	 * create string out of requested fields in this.config JSONObject
	 * to consider: had some issues with StringBuilder, used String in toReturn
	 * @param type keys based on response json, should be find in config
	 * @param fileName file name eg. "demo.jpg"
	 * @return
	 */
	private String createURL (String type, String fileName) {
		String toReturn = null;
		
		if (config != null) {
			JSONObject images = (JSONObject) config.get("images");
			JSONArray sizes = (JSONArray) images.get(type);
			int noOfSizes = sizes.size();			
			toReturn = (String) images.get("base_url");
			if (noOfSizes < 2) {
				toReturn += ((String)sizes.get(0)); 	// in case only original size...
			} else {
				toReturn += ((String)sizes.get(sizes.size()-2)); 	// second from end is biggest
			}
			toReturn += "/" + fileName;
		}
		return toReturn;
	}
	
	/**
	 * creating file path from selected directory, hardcoded values relevant for each search type
	 * and fileName (should be without leading /, that caused some problems before)
	 * @param type
	 * @param fileName
	 * @return
	 */
	private String downloadFile(String type, String fileName) {
		String toReturn = null;
		String localPath = MovieExplorer.getSelectedDirectory();	// here not null
		switch (type) {
		case "still_sizes":
			localPath += "/w300/";
			break;
		case "profile_sizes":
			localPath += "/h632/";
			break;
		case "poster_sizes":
			localPath += "/w780/";
			break;
		case "logo_sizes":
			localPath += "/w500/";
			break;
		case "backdrop_sizes":
			localPath += "/w1280/";
			break;
		default:
			break;
		}
		localPath += fileName;		// here complete local file name
		
		File localFile = new File(localPath);
		if (localFile.exists()) {
			toReturn = localPath;
			System.out.println("using local file");
		} else {
			String urlToAPI = createURL(type, fileName);
			try {
				// from https://stackoverflow.com/questions/921262/how-to-download-and-save-a-file-from-internet-using-java
				URL webSource = new URL(urlToAPI);
				ReadableByteChannel rbc = Channels.newChannel(webSource.openStream());
				FileOutputStream fos = new FileOutputStream(localPath);
				fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
				// end of copy
				toReturn = localPath;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return toReturn;
	}

}
