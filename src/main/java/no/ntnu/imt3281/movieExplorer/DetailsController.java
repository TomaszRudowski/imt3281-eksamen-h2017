package no.ntnu.imt3281.movieExplorer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class DetailsController {

    @FXML private Label filmTitle;

    @FXML private ImageView filmPict;

    @FXML private TextArea filmOverview;

    @FXML private ListView<String> filmGenres;

    /**
     * #7 method to display film details from a JSON
     * @param detailJSON
     */	
	public void displayDetails(JSON detailJSON) {
		TheMovieDBConfiguration configuration = new TheMovieDBConfiguration(); // based on API or db
		
		JSONObject jsonObj = (JSONObject) detailJSON.getDataObject();
		String title = (String) jsonObj.get("title");
		//switched to use of TheMovieDBConfiguration
		String pictPath2 = (String) jsonObj.get("poster_path"); 
		if (pictPath2.startsWith("/")) {
			// need to skip leading "\/" in picture path, two char including escape'\'
			// to avoid http://image.tmdb.org/t/p/w780//path.jpg
			pictPath2 = pictPath2.substring(1);  
		}			
		
		String overview = (String) jsonObj.get("overview");
		JSONArray genres = (JSONArray) jsonObj.get("genres");
		
		// from project 2 ChatRoomController line 221 (decided on list instead of for example textArea)
		ObservableList<String> items = FXCollections.observableArrayList();
		for (int i = 0; i < genres.size(); i++) {
			JSONObject elem = (JSONObject) genres.get(i);
			items.add((String) elem.get("name"));
		}
		filmGenres.setItems(items);
		//consider	Platform.runLater(() ->	 filmGenres.setItems(items));		
		filmTitle.setText(title);
		filmOverview.setText(overview);
				
		String pictURL2 = configuration.getPosterURL(pictPath2);
				
		Image picture = new Image(pictURL2, 200, 300, true, false);		
		Platform.runLater(() -> { filmPict.setImage(picture); });
	}

}
