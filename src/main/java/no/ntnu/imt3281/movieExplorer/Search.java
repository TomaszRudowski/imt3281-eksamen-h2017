package no.ntnu.imt3281.movieExplorer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Class to task 2. Fetches json data through GET requests.
 * @author Tomasz Rudowski
 *
 */
public class Search {
	private static FilmDatabase filmDB;
	
	/** !! MOVED TO RequestAPI interface !!
	 * based on CallTheMovieDB.java common for all requests
	 * @param req
	 * @return
	 */
	@Deprecated
	private static JSON sendRequest(String req) {
		JSON toReturn = null;
		// from CallTheMovieDB.java example
		try {
			String response = Unirest.get(req).asString().getBody();
			//System.out.println(response);		// Outputs JSON response from server.
			//Unirest.shutdown();			// Must be called at the end of the application
			
			JSONParser parser = new JSONParser();
			JSONObject jsonResp = (JSONObject) parser.parse(response);
			toReturn = new JSON(jsonResp);
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return toReturn;
	}

	/**
	 * Encoding search string to avoid spaces etc. 
	 * Create URL string and calling sendRequest().
	 * @param string query string
	 * @return JSON object built from GET response
	 */
	public static JSON multiSearch(String string) {	
		// task #8
		
		filmDB = new FilmDatabase();
		filmDB.openConnecion();	
		JSON toReturn = filmDB.getFromMovies(string);
		
		
		if (toReturn == null) {	// not found in db
			System.out.println(string  + " not found in db");
			// from https://stackoverflow.com/questions/24229660/replacing-spaces-with-20-in-java
			String encodedString = null;
				try {
					encodedString = URLEncoder.encode(string, "UTF-8");
				    //System.out.println(encodedString);
				} catch (UnsupportedEncodingException ignored) {
				    // Can be safely ignored because UTF-8 is always supported
				}
			// end of copy
			
			// https://api.themoviedb.org/3/search/multi?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US&query=QUERY&page=1&include_adult=false
			String req = "https://api.themoviedb.org/3/search/multi?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US&query="
					+ encodedString 
					+ "&page=1&include_adult=false";
			toReturn = RequestAPI.sendRequest(req);
			if (toReturn != null) {	// update db
				int test = filmDB.addToMoviesTable(string, toReturn);
				System.out.println("added to movies "+test);
			}			
		}		
		
		filmDB.closeConnection();
		return toReturn;
	}	

	/**
	 * Create URL string and calling sendRequest(). 
	 * @param string query string
	 * @return JSON object built from GET response
	 */
	public static JSON actors(long filmID) {
		// task #8
		
		filmDB = new FilmDatabase();
		filmDB.openConnecion();	
		JSON toReturn = filmDB.getFromActors(filmID);		
		if (toReturn == null) {
			// https://api.themoviedb.org/3/movie/{movie_id}/credits?api_key=a47f70eb03a70790f5dd711f4caea42d			
			String req = "https://api.themoviedb.org/3/movie/"
					+ filmID
					+ "/credits?api_key=a47f70eb03a70790f5dd711f4caea42d";
			
			toReturn = RequestAPI.sendRequest(req);
			if (toReturn != null) {	// update db
				int test = filmDB.addToActorsTable(filmID, toReturn);
				System.out.println("added to actors "+test);
			}
		}		
		filmDB.closeConnection();
		return toReturn;		
	}

	/**
	 * Create URL string and calling sendRequest(). 
	 * @param string query string
	 * @return JSON object built from GET response
	 */
	public static JSON takesPartIn(int i) {
		// task #8
		
		filmDB = new FilmDatabase();
		filmDB.openConnecion();	
		JSON toReturn = filmDB.getFromParts(i);
				
		if (toReturn == null) {
			String req = "https://api.themoviedb.org/3/discover/movie?api_key=a47f70eb03a70790f5dd711f4caea42d"
					+ "&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1"
					+ "&with_people=" + Integer.toString(i);
			
			toReturn = RequestAPI.sendRequest(req);		
			if (toReturn != null) {	// update db
				int test = filmDB.addToPartsTable(i, toReturn);
				System.out.println("added to parts "+test);
			}
		}
		filmDB.closeConnection();
		return toReturn;
	}
	
	/**
	 * Task #7 get detail information about a movie with given id
	 * @param id
	 * @return
	 */
	public static JSON movie(long id) {
		// https://api.themoviedb.org/3/movie/{movie_id}?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US
		String req = "https://api.themoviedb.org/3/movie/"
				+ id
				+ "?api_key=a47f70eb03a70790f5dd711f4caea42d&language=en-US";		
		return RequestAPI.sendRequest(req);
		
	}

}
